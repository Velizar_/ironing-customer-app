Rails.application.routes.draw do
  root 'home#index'
  post 'home#toggle_dark_mode' => 'home#toggle_dark_mode', as: :toggle_dark_mode
  post 'push_notification_address' => 'home#create_push_notification_address'

  resources :registration, only: [:new, :create]
  get 'registration/link'
  post 'registration/submit_phone' => 'registration/submit_phone', as: :submit_phone
  get 'registration/set_password'
  post 'registration/complete_account' => 'registration/complete_account', as: :complete_account

  post 'login/' => 'login#login'
  delete 'logout/' => 'login#logout'

  get 'request/new' => 'booking_requests#new', as: :new_request
  post 'request' => 'booking_requests#create', as: :create_request
  delete 'request/:id' => 'booking_requests#delete', as: :delete_request
  delete 'request/booking/:id' => 'booking_requests#delete_booking', as: :delete_booking
end

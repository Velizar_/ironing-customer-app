# Create an AMI for the customer app server

packer {
  required_plugins {
    amazon = {
      version = "= 1.2.1"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

# Speed up AMI creation
# Downside: there might be leftovers (files, packages) which Ansible won't remove
variable "reuse_last_ami" {
  type    = bool
  default = true
}

variable "env" {
  type = string
}

# Uses t3a (which is on x86_64) instead of t4g (which is on arm64)
# The reason for this is that Rocky 9 on arm64 doesn't boot with 600MB of RAM, only with 625MB+
# That is because EFI Stub wants to allocate too much memory for the kernel image.
source "amazon-ebs" "rocky" {
  ami_name      = "customer-app-${var.env}"
  instance_type = "t3a.small"
  region        = "eu-west-2"
  source_ami_filter {
    filters = {
      name                = var.reuse_last_ami ? "customer-app-${var.env}" : "Rocky-9-EC2-Base-9.1-*.x86_64"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = var.reuse_last_ami ? ["self"] : ["792107900819"]
  }
  ssh_username = "rocky"
  iam_instance_profile = "customer_app_server_profile_${var.env}"
  tags = {
    Name     = "Customer app ${var.env}"
    # Make it easier for someone looking in AWS to find out what created the resource
    Provider = "Packer"
    Filename = "aws-rocky.pkr.hcl"
  }
  force_deregister = true
  force_delete_snapshot = true

  launch_block_device_mappings {
    volume_type           = "gp3"
    device_name           = "/dev/sda1"
    delete_on_termination = true
    volume_size           = 10
  }
}

build {
  name    = "customer-app-instance"
  sources = [
    "source.amazon-ebs.rocky"
  ]

  provisioner "ansible" {
    playbook_file        = "../ansible/playbook.yml"
    galaxy_file          = "../ansible/requirements.yml"
#    galaxy_force_install = true # allow changing versions in requirements.yml
    sftp_command         = "/usr/libexec/openssh/sftp-server -e"
    # An alternative workaround involves `use_proxy = false` but then it can't auth
    ansible_ssh_extra_args = [
      # workaround for https://github.com/hashicorp/packer-plugin-ansible/issues/69
      "-oHostKeyAlgorithms=+ssh-rsa -oPubkeyAcceptedKeyTypes=+ssh-rsa"
    ]
    ansible_env_vars = ["ANSIBLE_CONFIG=../ansible/ansible_pipelining.cfg"]
    extra_arguments  = [
      # workaround for Ansible failing due to being unable to send a file
      "--scp-extra-args",
      "'-O'",
      "--extra-vars",
      "hosts_variable=default env=${var.env}"
    ]
  }
}

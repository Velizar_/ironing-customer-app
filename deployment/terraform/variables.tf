variable "environment" {
  description = "What is the name of the environment (staging/production)"
}

variable "secret_zone_name" {
  description = "The name of the secret Route 53 zone"
}

variable "secret_zone_subdomain" {
  description = "The subdomain under secret_zone_name, for the customer app only"
}

variable "aws_region" {
  description = "AWS region to launch instances in"
  default     = "eu-west-2"
}

variable "instance_size" {
  description = "AWS name for the EC2 instance type"
  default     = "t3a.nano"
}

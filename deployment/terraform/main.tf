# Infrastructure surrounding the main instance goes in this file

# Built and managed with Packer - see aws-rocky.pkr.hcl
data "aws_ami" "main_ami" {
  most_recent = true
  owners      = ["self"]

  filter {
    name   = "tag:Name"
    values = ["Customer app ${var.environment}"]
  }
}

# Built with Packer in the backend project
data "aws_ebs_volume" "database_volume" {
  most_recent = true
  filter {
    name   = "tag:Name"
    values = ["Backend DB ${var.environment}"]
  }
}

data "aws_route53_zone" "secret_zone" {
  name = var.secret_zone_name
  count = var.environment == "staging" ? 1 : 0
}

data "aws_cloudwatch_log_group" "main_log" {
  name = "Main_log_${var.environment}"
}

resource "aws_instance" "main_server" {
  ami           = data.aws_ami.main_ami.id
  instance_type = var.instance_size
  availability_zone = data.aws_ebs_volume.database_volume.availability_zone
  iam_instance_profile = aws_iam_instance_profile.customer_app_server_profile.name

  tags = {
    Name     = "Customer-app-${var.environment}"
    # Make it easier for someone looking in AWS to find out what created the resource
    Provider = "Terraform"
    Filename = "main.tf"
  }
}

# Note: the domain zone for production is managed by Superhosting
# If we ever destroy this elastic IP, the new one needs to be manually configured again
resource "aws_eip" "main_eip" {
  instance = aws_instance.main_server.id
}

resource "aws_route53_record" "www" {
  zone_id = data.aws_route53_zone.secret_zone[0].zone_id
  name    = var.secret_zone_subdomain
  type    = "A"
  ttl     = 300
  records = [aws_eip.main_eip.public_ip]
  count   = var.environment == "staging" ? 1 : 0
}

resource "aws_cloudwatch_log_stream" "customer_app_log_stream" {
  name           = "Customer app"
  log_group_name = data.aws_cloudwatch_log_group.main_log.name
}

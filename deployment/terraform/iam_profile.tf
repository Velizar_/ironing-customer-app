# Everything about permissions goes in this file:
# IAM roles, policies, profiles

# Managed in the backend repository
data "aws_iam_policy" "allow_writing_to_logs_policy" {
  name = "main_server_role_policy_${var.environment}"
}

resource "aws_iam_role" "customer_app_server_role" {
  name = "customer_app_server_role_${var.environment}"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_policy" "read_access_to_s3_bucket_policy" {
  name   = "read_access_to_s3_bucket_policy_${var.environment}"
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "AllObjectActions",
        "Effect": "Allow",
        "Action": [
          "s3:*Object",
          "s3:ListBucket",
          "s3:ListObjectsV2",
        ],
        "Resource": [
          "arn:aws:s3:::certificates-${var.environment}",
          "arn:aws:s3:::certificates-${var.environment}/*",
        ]
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "logs_policy_attachment" {
  role       = aws_iam_role.customer_app_server_role.name
  policy_arn = data.aws_iam_policy.allow_writing_to_logs_policy.arn
}

resource "aws_iam_role_policy_attachment" "s3_policy_attachment" {
  role       = aws_iam_role.customer_app_server_role.name
  policy_arn = aws_iam_policy.read_access_to_s3_bucket_policy.arn
}

resource "aws_iam_instance_profile" "customer_app_server_profile" {
  name = "customer_app_server_profile_${var.environment}"
  role = aws_iam_role.customer_app_server_role.name
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "ironing-customer-app-tfstate"

  lifecycle {
    prevent_destroy = true
  }

  tags = {
    Name     = "Remote Terraform state for the customer app"
    Provider = "Terraform"
    Filename = "terraform-state/terraform-state.tf"
  }
}

resource "aws_s3_bucket_versioning" "versioning" {
  bucket = aws_s3_bucket.terraform_state.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_lifecycle_configuration" state_noncurrent_version_transition {
  bucket = aws_s3_bucket.terraform_state.id

  rule {
    id = "state_noncurrent_version_transition"
    filter {
      prefix = "logs/"
    }
    status = "Enabled"

    noncurrent_version_transition {
      noncurrent_days = 30
      storage_class   = "GLACIER"
    }
  }
}

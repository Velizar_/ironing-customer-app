terraform {
  required_providers {
    aws = {
      version = "~> 4.55"
    }
  }
  required_version = "1.3.9"
}

provider "aws" {
  region = "eu-west-2"
}

#!/bin/bash
# CI/CD - merges the current branch into staging or production and then deploys

if [[ "$#" -lt 1 || "$#" -gt 4 ]]; then
    echo "Please provide from one to four arguments"
    echo "Usage: ./deploy.sh environment [recreate] [skip tests] [use fresh AMI]"
    echo "recreate:      true|false, default is false - deploy to a new AMI instead of using Ansible to update the instances in-place"
    echo "skip tests:    true|false, default is false - do not run rails test and rails spec"
    echo "use fresh AMI: true|false, default is false - create the AMI from Rocky's image instead of building on top of our latest AMI - slower, but starts clean (avoids leftovers) and uses their latest AMI. Best used with recreate."
    echo "For example: ./deploy.sh staging false true # environment is staging, do not recreate, skip tests, do not use fresh AMI"
    exit 0
fi

ENVIRONMENT=$1
RECREATE=$2
SKIP_TESTS=$3
REUSE_LAST_AMI=true
if [[ $4 == 'true' ]]; then
  REUSE_LAST_AMI=false
fi

if `! git rev-parse --verify $ENVIRONMENT >& /dev/null`; then
  echo "Error: Nonexistent branch $ENVIRONMENT - if this is not a typo then please create the branch manually"
  exit 1
fi

# run tests (fail on exit)
set -e
if [[ $SKIP_TESTS != 'true' ]]; then
  echo 'No tests to run, moving on'
fi

# merge main into the branch and push it
git fetch . main:$ENVIRONMENT
git push bitbucket $ENVIRONMENT

# deploy
packer init "$(dirname "$0")"/packer/aws-rocky.pkr.hcl
if [[ $RECREATE == 'true' ]]; then
  # Deploy the code to a new AMI then spin up a new EC2 instance with the new AMI
  cd "$(dirname "$0")"/packer
  packer build -var "env=$ENVIRONMENT" -var "reuse_last_ami=$REUSE_LAST_AMI" aws-rocky.pkr.hcl
  cd ../terraform
  ./tf-any.sh $ENVIRONMENT apply -auto-approve
else
  # Update the code via Ansible
  cd "$(dirname "$0")"/ansible
  ansible-playbook playbook.yml --extra-vars "hosts_variable=$ENVIRONMENT env=$ENVIRONMENT"
  echo 'Build deployed! Now running Packer build to update the AMI.'
  cd ../packer
  packer build -var "env=$ENVIRONMENT" -var "reuse_last_ami=$REUSE_LAST_AMI" aws-rocky.pkr.hcl
fi

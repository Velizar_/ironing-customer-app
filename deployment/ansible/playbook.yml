---
- name: Provision customer app
  hosts: "{{ hosts_variable }}"
  become: true
  pre_tasks:
    - name: Read the Ruby version file into a variable
      set_fact:
        ruby_version: "{{ lookup('file', '../../.ruby-version') }}"

    - name: Read the domain name JSON file into a variable
      set_fact:
        domain_name: "{{ (lookup('file', '../terraform/environments/' ~ env ~ '_secrets.tfvars.json') | from_json)['secret_zone_subdomain']  ~ '.' ~
          (lookup('file', '../terraform/environments/' ~ env ~ '_secrets.tfvars.json') | from_json)['secret_zone_name'] }}"

    - name: Enable EPEL to get access to the awscli package
      package:
        name: epel-release

    - name: Install packages from CRB
      ansible.builtin.dnf:
        name:
          - libyaml-devel # required for compiling Ruby 3.2.x
          - awscli # download the certificates from S3
        enablerepo: crb
        state: present

    # rvm.ruby works without this, but this step speeds up the playbook by several minutes
    # The keyservers that rvm.ruby uses are very brittle. Plus their fetching of gpg keys is not idempotent,
    # exposing ansible to network failures even when the key is already imported. Also, there is
    # https://github.com/rvm/rvm1-ansible/issues/196
    - name: Import GPG keys from rvm.io
      shell: |-
        if ! gpg2 --list-keys {{ item.id }}; then
          curl -sSL https://rvm.io/{{ item.file }} | gpg2 --batch --import -
        fi
      become_user: 'rocky'
      with_items:
        - id: 409B6B1796C275462A1703113804BB82D39DC0E3
          file: mpapis.asc
        - id: 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
          file: pkuczynski.asc
      register: gpg_result
      changed_when: '(gpg_result.rc == 0) and ("imported: 1" in gpg_result.stderr)'

    - name: 'Trick rvm.ruby role into not importing keys from keyservers again'
      command: 'true'
      changed_when: false
      register: gpg_import

  roles:
    # Add swap partition because we have spare space anyway and dnf update probably needs it
    - role: geerlingguy.swap
      vars:
        swap_file_size_mb: '512'
        swap_file_path: '/swapfile'
        swap_swappiness: '1'
    # Use RVM to ensure the exactly right version of Ruby is being installed
    - { role: rvm.ruby,
        tags: ruby,
        rvm1_rubies: [ 'ruby-{{ ruby_version }}' ],
        rvm1_user: 'rocky'
    }

  tasks:
    - name: Upload SSH public keys to allow SSH-ing into the EC2 instance
      ansible.builtin.copy:
        src: "../files_to_upload/authorized_keys"
        dest: "/home/rocky/.ssh/authorized_keys"

    - name: Upload environment variables files
      ansible.builtin.copy:
        src: "../files_to_upload/{{ env }}_environment"
        dest: "/etc/environment"

    - name: Upload Puma service file (for the app at ~/app) to allow manipulating it as a systemd service
      ansible.builtin.copy:
        src: "../files_to_upload/puma.service"
        dest: "/etc/systemd/system/puma.service"

    - name: Install packages
      package:
        name:
          - git # clone the app repo and acme.sh
          - nginx
          - nodejs # Rails needs a JS runtime
          - dnf-automatic # automatic security updates
          - socat # web server used by acme.sh to pass the let's encrypt HTTP-01 challenge
        state: present

    - name: Gather information about installed packages
      ansible.builtin.package_facts:
        manager: auto

    - name: Install Amazon Cloudwatch agent
      dnf:
        name: https://s3.eu-west-2.amazonaws.com/amazoncloudwatch-agent-eu-west-2/redhat/amd64/latest/amazon-cloudwatch-agent.rpm
        state: present
        disable_gpg_check: true
      when: "'amazon-cloudwatch-agent' not in ansible_facts.packages"

    - name: Upload Nginx config file
      ansible.builtin.template:
        src: "../files_to_upload/nginx.conf.j2"
        dest: "/etc/nginx/nginx.conf"

    - name: Upload dnf-automatic config file
      ansible.builtin.copy:
        src: "../files_to_upload/dnf-automatic.conf"
        dest: "/etc/dnf/automatic.conf"

    - name: Upload Amazon Cloudwatch config file
      ansible.builtin.template:
        src: "../files_to_upload/amazon-cloudwatch-agent.json.j2"
        dest: "/opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json"

    - name: Get codebase from git
      become: false
      git:
        repo: https://Velizar_@bitbucket.org/Velizar_/ironing-customer-app.git
        dest: /home/rocky/app
        version: "{{ env }}"

    - name: Install gems from the Gemfile
      become: false
      community.general.bundler:
        state: present
        chdir: /home/rocky/app
        executable: /home/rocky/.rvm/wrappers/default/bundle

    - name: Precompile assets
      become: false
      shell:
        cmd: set -a && source /etc/environment && set +a && /home/rocky/.rvm/wrappers/default/rails assets:precompile
        chdir: /home/rocky/app

    - name: Allow socat to bind ports
      community.general.capabilities:
        path: /usr/bin/socat
        capability: cap_net_bind_service+ep
        state: present

    # The version needs to be incremented manually in order to keep acme.sh up to date
    - name: Get acme.sh from git
      become: false
      git:
        repo: https://github.com/acmesh-official/acme.sh.git
        dest: /home/rocky/acme.sh
        single_branch: yes
        version: 3.0.5
      register: get_acme

    - name: Install acme.sh
      become: false
      shell:
        cmd: bash acme.sh --install -m velizar.hs@gmail.com
        chdir: /home/rocky/acme.sh
      when: get_acme.changed

    # These files are initially created manually, then auto-renewed
    - name: Download files for acme.sh and certificates
      become: false
      shell: "aws s3 sync s3://certificates-{{ env }}/{{ domain_name }} /home/rocky/.acme.sh/{{ domain_name }}"

    - name: Set tight permissions for TLS private key file
      ansible.builtin.file:
        path:  "/home/rocky/.acme.sh/{{ domain_name }}/{{ domain_name }}.key"
        state: file
        mode: '0600'
        owner: rocky
        group: rocky

    - name: Upload script for renewing the TLS certificate
      become: false
      ansible.builtin.template:
        src: ../files_to_upload/renew-certificate.sh.j2
        dest: /home/rocky/renew-certificate.sh
        mode: '0755'

    - name: Create a cron job for auto-renewing the TLS certificate
      ansible.builtin.cron:
        name: renew the TLS certificate
        job: /home/rocky/renew-certificate.sh
        user: rocky
        minute: "2"
        hour: 1,13

    - name: Enable nginx
      service:
        name: nginx
        enabled: yes

    - name: Enable puma
      service:
        name: puma
        enabled: yes

    - name: Enable dnf-automatic
      service:
        name: dnf-automatic.timer
        enabled: yes

    - name: Enable AWS CloudWatch logs
      service:
        name: amazon-cloudwatch-agent
        enabled: yes

    - name: Restart puma if applying playbook to an already live instance, rather than building an AMI
      service:
        name: puma
        state: restarted
      when: hosts_variable != 'default'

    # by default crashkernel is taking too much RAM
    # SELinux causes problems when the Puma service tries to start the Puma executable
    - name: Update the kernel arguments to reduce crashkernel RAM and disable SELinux
      shell: grubby --update-kernel=ALL --args="crashkernel=300M-1G:27M,1G-4G:130M,4G-64G:320M,64G-:576M selinux=0"

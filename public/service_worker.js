self.addEventListener('push', event => {
    const data = event.data.json()
    const title = data.title
    delete data.title
    event.waitUntil(self.registration.showNotification(title, data))
})

self.addEventListener('notificationclick', function(event) {
    if (event.notification.data.url) {
        event.notification.close()
        event.waitUntil(clients.openWindow(event.notification.data.url))
    }
})

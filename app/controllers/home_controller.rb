include ApplicationHelper

class HomeController < ApplicationController

  # GET /
  def index
    if session[:auth_token].present?
      res = BookingInfoFetcher.fetch(session[:auth_token])
      if res.nil?
        authentication_expired
      else
        @booking_info = res
      end
    else
      render 'unauthenticated'
    end
  end

  # POST /push_notification_address
  def create_push_notification_address
    CustomerRegistrator.register_push_notification(push_notification_address_params.to_h, session[:auth_token])
  end

  # POST /toggle_dark_mode
  def toggle_dark_mode
    par = params.permit(:darkMode)[:darkMode]
    p par
    p params
    if par == 'on'
      cookies[:dark_mode] = 'on'
    else
      cookies.delete(:dark_mode)
    end
    redirect_to root_path
  end

  private

  def push_notification_address_params
    params.permit(:endpoint, :auth_key, :p256dh_key)
  end
end

class ApplicationController < ActionController::Base
  def authentication_expired
    session.delete(:auth_token)
    redirect_to root_path, warn: 'Session expired, please login again.'
  end

  def redirect_if_tokenless
    redirect_to root_path if session[:auth_token].blank?
  end
end

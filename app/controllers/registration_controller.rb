include ApplicationHelper

class RegistrationController < ApplicationController

  # GET /registration/new
  def new
    @available, @not_confirmed_as_available,
      @unavailable, @unconfirmed = BookingRequest.get_booking_availability
  end

  # POST /registration
  def create
    par = registration_params
    par['collection_dates'] = params[:collection_dates]&.split(',')
    par['delivery_dates'] = params[:delivery_dates]&.split(',')
    if CustomerRegistrator.register(par).present?
      redirect_to root_path, notice: 'Registration successful - we will contact you when your account is activated.'
    else
      redirect_to root_path, alert: 'Sorry, there has been an error registering your account, please contact us or try again.'
    end
  end

  # GET /registration/link
  def link
  end

  # POST /registration/submit_phone
  def submit_phone
    if params[:phone].blank?
      redirect_to registration_link_path, alert: 'Please provide a mobile phone number'
      return
    end

    res = CustomerRegistrator.create_code(params[:phone])
    case res
    in 'Success'
      redirect_to root_path, notice: 'Please check your phone for a text with a confirmation code'
    in 'Customer not found'
      redirect_to registration_link_path, alert: "Sorry, we don't have this phone number in our database. Please try again or contact us."
    in ['Try again', seconds_to_wait]
      redirect_to registration_link_path, alert: "You have already requested a code. Please try again in #{seconds_to_wait} seconds if you do not receive a text."
    else
      redirect_to registration_link_path, alert: res
    end
  end

  # GET /registration/set_password
  def set_password
    if params[:phone].blank? || CustomerRegistrator.check_code(params[:code], params[:phone]) != "Success"
      redirect_to root_path, alert: 'Invalid URL, please request a new code'
    end
  end

  # POST /registration/complete_account
  def complete_account
    if params[:password].blank? or params[:phone].blank? or params[:code].blank?
      redirect_to root_path, alert: 'Sorry, something went wrong - please try again.'
      return
    end
    res = CustomerRegistrator.complete_account(params[:code], params[:phone], params[:password])
    if res == 'Success'
      redirect_to root_path, notice: 'Registration successful. You may now log in.'
    else
      redirect_to root_path, alert: 'Sorry, something went wrong - please try again.'
    end
  end

  private

  # plus params[:collection_dates] and params[:delivery_dates]
  def registration_params
    params.require(:customer_registration).permit(
      :first_name, :last_name, :phone_mobile, :phone_home, :email, :password,
      :payment_scheme, :address, :post_code, :additional_requirements)
  end
end

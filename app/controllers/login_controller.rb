include ApplicationHelper

class LoginController < ApplicationController

  # POST /login
  def login
    res = Authenticator.login(params[:phone], params[:password])
    if res.nil?
      redirect_to login_path, alert: 'Incorrect phone number or password'
    else
      session[:auth_token] = res
      redirect_to root_path, notice: 'Successfully logged in'
    end
  end

  # POST /logout
  def logout
    Authenticator.logout(session[:auth_token])
    session.delete(:auth_token)
    redirect_to root_path, notice: 'Successfully logged out'
  end
end

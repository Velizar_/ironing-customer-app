include ApplicationHelper

class BookingRequestsController < ApplicationController
  before_action :redirect_if_tokenless

  # GET /request/new
  def new
    @available, @not_confirmed_as_available,
      @unavailable, @unconfirmed = BookingRequest.get_booking_availability
  end

  # POST /request
  def create
    par = params.permit(:collection_dates, :delivery_dates, :additional_requirements)
    if par[:collection_dates].blank?
      redirect_to root_path, alert: 'Sorry, there has been an error parsing the dates, please try again.'
    end
    notice_msg = 'Request successfully sent.'
    if params['selected-uncertain'] != 'true' && params['additional_requirements'].blank?
      notice_msg = "Booked ironing on #{Date.strptime(par[:collection_dates], "%m/%d/%Y").to_fs}."
    end
    par[:collection_dates] = par[:collection_dates]&.split(',')
    par[:delivery_dates] = par[:delivery_dates]&.split(',')
    if BookingRequest.create(par, session[:auth_token]).nil?
      authentication_expired
    else
      redirect_to root_path, notice: notice_msg
    end
  end

  # DELETE /request/1
  def delete
    if BookingRequest.delete(params[:id], session[:auth_token]).nil?
      authentication_expired
    else
      redirect_to root_path, notice: 'Request cancelled.'
    end
  end

  # DELETE /request/booking/1
  def delete_booking
    if BookingRequest.delete_booking(params[:id], session[:auth_token]).nil?
      authentication_expired
    else
      redirect_to root_path, notice: 'Booking cancelled.'
    end
  end
end

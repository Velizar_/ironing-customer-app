//= require bootstrap
//= require jquery
//= require jquery_ujs

// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails
import "@hotwired/turbo-rails"
import "controllers"
import jQuery from "jquery"
window.jQuery = jQuery
window.$ = jQuery
import "bootstrap-datepicker"

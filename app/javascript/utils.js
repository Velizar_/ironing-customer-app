export function phoneIsValidUk(phone, isLandline) {
    let remaining = phone
    if (phone.startsWith('+44'))
        remaining = remaining.slice(3)
    else if (phone.startsWith('0'))
        remaining = remaining.slice(1)
    else
        return "Please enter a UK number - starting with +44 or with 0."
    if (!/^\d*$/.test(remaining))
        return "Please enter a phone number containing only digits"
    if (isLandline) {
        if (!['1', '2', '3', '5'].includes(remaining[0]))
            return "Please enter a landline number starting with 01, 02, 03, or 05."
        if (remaining.length < 9)
            return "Your phone number is too short, it needs to contain at least nine digits after the 0."
        if (remaining.length > 10)
            return "Your phone number is too long, it needs to contain at most ten digits after the 0."
        return true
    } else {
        if (remaining[0] !== '7')
            return "Please enter a phone number starting with 07."
        if (remaining.length < 10)
            return "Your phone number is too short, it needs to contain ten digits after the 0."
        if (remaining.length > 10)
            return "Your phone number is too long, it needs to contain ten digits after the 0."
        return true
    }
}

export function updateValidity(jquerySelector, isValid) {
    if (isValid)
        jquerySelector.removeClass('is-invalid')
    else
        jquerySelector.addClass('is-invalid')
}

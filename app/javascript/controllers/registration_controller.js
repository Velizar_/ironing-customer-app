import {Controller} from "@hotwired/stimulus"
import {phoneIsValidUk, updateValidity} from "utils";

export default class extends Controller {
  onPasswordInput(_event) {
    const $password = $('#customer_registration_password')
    const passwordLongEnough = $password.val().length >= 6
    updateValidity($password, passwordLongEnough)
    return passwordLongEnough
  }

  onPasswordConfirmInput(_event) {
    const $passwordConfirmation = $('#customer_registration_password_confirmation')
    const passwordConfirmMatches = $('#customer_registration_password').val() === $passwordConfirmation.val()
    updateValidity($passwordConfirmation, passwordConfirmMatches)
    return passwordConfirmMatches
  }

  onPhoneMobileInput(_event) {
    const $phoneMobile = $('#customer_registration_phone_mobile')
    const mobilePhoneIsValid = phoneIsValidUk($phoneMobile.val(), false)
    updateValidity($phoneMobile, mobilePhoneIsValid === true)
    $('#invalid-phone-mobile').text(mobilePhoneIsValid)
    return mobilePhoneIsValid === true
  }

  onPhoneHomeInput(_event) {
    const $phoneHome = $('#customer_registration_phone_home')
    if ($phoneHome.val() === '') {
      updateValidity($phoneHome, true)
      return true
    }
    const homePhoneIsValid = phoneIsValidUk($phoneHome.val(), true)
    updateValidity($phoneHome, homePhoneIsValid === true)
    $('#invalid-phone-home').text(homePhoneIsValid)
    return homePhoneIsValid === true
  }

  connect() {
    const $password = $('#customer_registration_password')
    const $passwordConfirmation = $('#customer_registration_password_confirmation')
    const $phoneMobile = $('#customer_registration_phone_mobile')
    const $phoneHome = $('#customer_registration_phone_home')
    // hide leftover from the booking requests partial
    $('#additional-requirements-info').hide()

    // on form submit, validate and if it fails display any failures
    $('#registration-form').submit((_e) => {
      const passwordValid = this.onPasswordInput(_e) && this.onPasswordConfirmInput(_e)
      const phoneMobileValid = this.onPhoneMobileInput(_e)
      const phoneHomeValid = this.onPhoneHomeInput(_e)
      const collectionDatePresent = $('#request_datepicker_input').val() !== ''
      $('#required-booking').attr('hidden', collectionDatePresent)
      const validationPassed = collectionDatePresent && passwordValid && phoneMobileValid && phoneHomeValid
      if (!validationPassed)
        window.scrollTo(0, 0)
      return validationPassed
    })
    // when typing in a text field with a validation error, update the error's status
    $password.on('input', (_event) => {
      if ($password.hasClass('is-invalid')) {
        this.onPasswordInput(_event)
      }
      if ($passwordConfirmation.hasClass('is-invalid')) {
        this.onPasswordConfirmInput(_event)
      }
    })
    $passwordConfirmation.on('input', (_event) => {
      if ($passwordConfirmation.hasClass('is-invalid')) {
        this.onPasswordConfirmInput(_event)
      }
    })
    $phoneMobile.on('input', (_event) => {
      if ($phoneMobile.hasClass('is-invalid')) {
        this.onPhoneMobileInput(_event)
      }
    })
    $phoneHome.on('input', (_event) => {
      if ($phoneHome.hasClass('is-invalid')) {
        this.onPhoneHomeInput(_event)
      }
    })
  }
}

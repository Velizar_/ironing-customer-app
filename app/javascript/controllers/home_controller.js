import {Controller} from "@hotwired/stimulus"

export default class extends Controller {

  promptRegisterForPushNotifications() {
    if (navigator.serviceWorker) {
      navigator.serviceWorker.register('/service_worker.js').then(async () => {
        const registration = await navigator.serviceWorker.ready
        const subscription = await registration.pushManager.getSubscription()
        const serverHasANotifAddress = $('#has-notification-address').data('value') === true
        // Probably a redundant check for serverHasANotifAddress, but it does not hurt
        if (subscription !== null && serverHasANotifAddress)
          return
        const permission = await Notification.requestPermission()
        if (permission === 'denied')
          return
        const newSubscription = await registration.pushManager.subscribe({
          userVisibleOnly: true,
          applicationServerKey: window.VAPID_PUBLIC_KEY
        })
        const subObj = JSON.parse(JSON.stringify(newSubscription))
        const response = await fetch('/push_notification_address', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          },
          body: JSON.stringify({
            endpoint: subObj.endpoint,
            auth_key: subObj.keys.auth,
            p256dh_key: subObj.keys.p256dh
          })
        })
        if (!response.ok) {
          // Retry quietly on page refresh
          newSubscription.unsubscribe()
        }
      }, err => {
        console.log('Register failed:')
        console.log(err)
      })
    }
  }

  connect() {
    $('.collapse').each((idx, obj) => {
      const collapse = $(obj)
      const btn = $('#btn-' + collapse.attr('id'))
      collapse.on('show.bs.collapse', () => {
        btn.html(btn.html().replace('Show', 'Hide'))
      })
      collapse.on('hide.bs.collapse', () => {
        btn.html(btn.html().replace('Hide', 'Show'))
      })
    })
    const $geocodes = $("#geocodes")
    if ($geocodes.data("present") === true) {
      const [dLat, dLng] = $geocodes.data("driver").split(',');
      const [cLat, cLng] = $geocodes.data("customer").split(',');
      const driverPoint = new google.maps.LatLng(dLat, dLng)
      const customerPoint = new google.maps.LatLng(cLat, cLng)
      const map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: customerPoint
      })
      const directionsService = new google.maps.DirectionsService
      const DirectionsRenderer = new google.maps.DirectionsRenderer({
        map: map,
        suppressMarkers: true
      })
      new google.maps.Marker({
        position: driverPoint,
        title: "Driver",
        map: map,
        icon: "https://maps.google.com/mapfiles/ms/icons/cabs.png"
      })
      new google.maps.Marker({
        position: customerPoint,
        title: "You",
        map: map
      })
      directionsService.route({
        origin: driverPoint,
        destination: customerPoint,
        travelMode: google.maps.TravelMode.DRIVING
      }, function (response, status) {
        if (status === google.maps.DirectionsStatus.OK) {
          DirectionsRenderer.setDirections(response);
        }
      })
    }

    // If push notifications are disabled, display the div asking the user to register for them
    const serverHasANotifAddress = $('#has-notification-address').data('value') === true
    if (navigator.serviceWorker) {
      navigator.serviceWorker.register('/service_worker.js').then(async () => {
        const permission = Notification.permission
        const cookieVal = document.cookie.split("; ")
            .find((row) => row.startsWith("dismissedNotifications="))
        const hasDismissed = cookieVal && cookieVal.split("=")[1] === 'true'
        const registration = await navigator.serviceWorker.ready
        const subscription = await registration.pushManager.getSubscription()
        const hasSubscription = subscription !== null
        if (permission === 'denied' || (
            permission === 'granted' && serverHasANotifAddress && hasSubscription)) {
          return
        }
        if (permission === 'granted' && !(serverHasANotifAddress && hasSubscription)) {
          // Generate a new notification without showing anything to the customer
          // This case should not happen in theory, but it looked like it happened a few times
          // Another case: there was a server error when subscribing, so this will retry quietly
          this.promptRegisterForPushNotifications()
        } else if (!hasDismissed) {
          // The user is not subscribed for notifications on this device
          // They may or may not be subscribed on another device
          $('#notifications-div').prop('hidden', '')
        }
      })
    }
    $('#notifications-btn').on('click', () => {
      this.promptRegisterForPushNotifications()
      $('#notifications-div').prop('hidden', 'hidden')
    })
    $('#dismiss-notifications-btn').on('click', () => {
      $('#notifications-div').prop('hidden', 'hidden')
      document.cookie = "dismissedNotifications=true"
    })
    $('#hide-notifications-btn').on('click', () => {
      $('#notifications-div').prop('hidden', 'hidden')
    })
    $('#darkModeSwitch').on('click', () => $('#dark-mode-form').submit())
  }
}

import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  connect() {
    $('#set-password-form').submit((event) => {
      const validates = $('#password').val() === $('#password_confirmation').val()
      if (!validates) {
        $('#password-mismatch-message').attr('hidden', false)
      }
      return validates
    })
  }
}

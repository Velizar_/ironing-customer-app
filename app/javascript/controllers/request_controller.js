import { Controller } from "@hotwired/stimulus"

/** States for delivery
 *
 * 1. User has not chosen a collection date:
 * - Label: Delivery is usually 2 days after collection.
 *
 * 2. User has just chosen a collection date and multiple delivery dates are available:
 * - Label: Delivery will be booked for 2022-06-30.
 * - Button saying "Looking for a different delivery date?" leading to a date picker
 *
 * 3. From state 2, user clicked "Looking for a different delivery date?"
 * - Label: Delivery will be booked for 2022-06-30.
 * - Button saying "Looking for a different delivery date?" leading to a date picker
 * - Button saying "More delivery dates (we might not be available on those)" leading to a date picker
 *
 * 4. User has chosen a collection date and only one delivery date is available:
 * - Label: Delivery will be booked for 2022-06-30.
 * - Button saying "More delivery dates (we might not be available on those)" leading to a date picker
 *
 * 5. User has chosen a collection date and no delivery dates are available, OR user has chosen multiple collection dates:
 * - Label: Delivery is usually 2 days after collection.
 * - Button saying "Select preferred delivery dates (optional)" leading to a date picker
 *
 * 6. User had chosen a multiple delivery dates, regardless of the collection dates:
 * - Label: Delivery requested for one of the dates you selected. We will contact you if we are not available on any of them.
 * - Which buttons are shown is determined by which state from 2-5 we're also in
 *
 * If user has entered additional requirements: delivery interface works the same way as before
 */

export default class extends Controller {
  // .toISOString() is off by one day in some time zones:
  // on my local machine, new Date("08/06/2018").toISOString() returns "2018-08-05T00:00:00.000Z"
  displayDateISO(date) {
    let month = date.getMonth() + 1 + ''
    if (month.length === 1) {
      month = '0' + month
    }
    let day = date.getDate() + ''
    if (day.length === 1) {
      day = '0' + day
    }

    return date.getFullYear() + "-" + month + "-" + day
  }

  setTextNoDeliveryChosen() {
    $('#delivery-initial-text').show()
    $('#delivery-date-text').hide()
    $('#delivery-multiple-dates-text').hide()
  }
  setTextDeliveryChosen() {
    $('#delivery-initial-text').hide()
    $('#delivery-date-text').show()
    $('#delivery-multiple-dates-text').hide()
  }
  setTextMultipleDeliveriesChosen() {
    $('#delivery-initial-text').hide()
    $('#delivery-date-text').hide()
    $('#delivery-multiple-dates-text').show()
  }

  setBtnTextToMoreDates() {
    $('#more-delivery-dates-options').show()
    $('#more-delivery-dates-btn').text('More delivery dates (we might not be available on those)')
  }
  setDeliveryOptionalDates() {
    $('#delivery-date-options').hide()
    $('#more-delivery-dates-options').show()
    $('#more-delivery-dates-btn').text('Select preferred delivery dates (optional)')
  }

  connect() {
    const datepicker = $('#request_datepicker')
    const datepickerUncertain = $('#datepicker-uncertain-days')
    const datepickerDelivery = $('#datepicker-delivery-days')
    const datepickerDeliveryExtra = $('#datepicker-more-delivery-days')
    const availability = $('#availability')
    const deliveryDatesCollapsible = $('#delivery-dates')
    const submitBtn = $('#submit-request')
    const datesAvailable = availability.data('available')
    const datesUnconfirmed = availability.data('unconfirmed')
    const datesNotConfirmedAvailable = availability.data('not-confirmed-as-available')
    const datesUnavailable = availability.data('unavailable')
    const datesAvailableObj = datesAvailable.map(x => new Date(x)).sort((a, b) => a.getTime() - b.getTime())
    const today = new Date()
    const in45Days = new Date()
    in45Days.setDate(today.getDate() + 44)
    datepicker.datepicker({
      startDate: today,
      endDate: in45Days,
      maxViewMode: 0,
      datesDisabled: datesNotConfirmedAvailable
    })
    datepickerUncertain.datepicker({
      startDate: today,
      endDate: in45Days,
      maxViewMode: 0,
      multidate: true,
      datesDisabled: datesUnavailable
    })
    datepickerDelivery.datepicker({
      startDate: today,
      endDate: in45Days,
      maxViewMode: 0,
      datesDisabled: datesNotConfirmedAvailable
    })
    datepickerDeliveryExtra.datepicker({
      startDate: today,
      endDate: in45Days,
      maxViewMode: 0,
      multidate: true,
      datesDisabled: datesUnavailable
    })
    datepicker.on('changeDate', () => {
      const date = datepicker.datepicker('getFormattedDate')
      $('#request_datepicker_input').val(date)
      $('#selected-uncertain').val(false)
      datepickerUncertain.datepicker('update', '')
      submitBtn.prop('disabled', date === '')

      const minDeliveryDate = new Date(date)
      minDeliveryDate.setDate(minDeliveryDate.getDate() + 2)
      const availableDeliveryDates = datesAvailableObj.filter(x => x.getTime() >= minDeliveryDate.getTime())
      const closestDeliveryDate = availableDeliveryDates[0]
      if (availableDeliveryDates.length === 0) {
        this.setTextNoDeliveryChosen()
        $('#request_datepicker_delivery_input').val('')
        this.setDeliveryOptionalDates()
      } else {
        this.setTextDeliveryChosen()
        $('#delivery-date-label').text(this.displayDateISO(closestDeliveryDate))
        $('#delivery-date-options').show()

        if (availableDeliveryDates.length > 1) {
          const tooEarlyAvailableDates = datesAvailable.filter(x => new Date(x).getTime() < minDeliveryDate.getTime())
          datepickerDelivery.datepicker('setDatesDisabled', tooEarlyAvailableDates.concat(datesNotConfirmedAvailable))
          if (!deliveryDatesCollapsible.hasClass('show') || !deliveryDatesCollapsible.is(':visible')) {
            $('#more-delivery-dates-options').hide()
          }
        } else {
          $('#delivery-date-options').hide()
          this.setBtnTextToMoreDates()
        }
        datepickerDelivery.datepicker('setDate', closestDeliveryDate)
      }
      const tooEarlyUnconfirmedDates = datesAvailable.concat(datesUnconfirmed).filter(x => new Date(x).getTime() < minDeliveryDate.getTime())
      datepickerDeliveryExtra.datepicker('setDatesDisabled', tooEarlyUnconfirmedDates.concat(datesUnavailable))
    })
    datepickerUncertain.on('changeDate', () => {
      const dates = datepickerUncertain.datepicker('getFormattedDate')
      $('#request_datepicker_input').val(dates)
      $('#selected-uncertain').val(true)
      datepicker.datepicker('update', '')
      submitBtn.prop('disabled', dates === '')

      $('#request_datepicker_delivery_input').val('')
      this.setTextNoDeliveryChosen()
      if (dates.length > 0) {
        this.setDeliveryOptionalDates()
        const minCollectionDate = dates.split(',').sort((d1, d2) => new Date(d1).getTime() - new Date(d2).getTime())[0]
        const minDeliveryDateObj = new Date(minCollectionDate)
        minDeliveryDateObj.setDate(minDeliveryDateObj.getDate() + 2)
        const tooEarlyDeliveryDates = datesAvailable.concat(datesUnconfirmed).filter(x => new Date(x).getTime() < minDeliveryDateObj.getTime())
        datepickerDeliveryExtra.datepicker('setDatesDisabled', datesUnavailable.concat(tooEarlyDeliveryDates))
      } else {
        $('#delivery-date-options').hide()
        $('#more-delivery-dates-options').hide()
      }
    })
    datepickerDelivery.on('changeDate', () => {
      const date = datepickerDelivery.datepicker('getFormattedDate')
      $('#request_datepicker_delivery_input').val(date)
      datepickerDeliveryExtra.datepicker('update', '')
      this.setTextDeliveryChosen()
      $('#delivery-date-label').text(this.displayDateISO(new Date(date)))
    })
    datepickerDeliveryExtra.on('changeDate', () => {
      const dates = datepickerDeliveryExtra.datepicker('getFormattedDate')
      $('#request_datepicker_delivery_input').val(dates)
      datepickerDelivery.datepicker('update', '')
      if (dates === '') {
        this.setTextNoDeliveryChosen()
      } else {
        this.setTextMultipleDeliveriesChosen()
      }
    })

    deliveryDatesCollapsible.on('shown.bs.collapse', () => {
      this.setBtnTextToMoreDates()
    })
    deliveryDatesCollapsible.on('hide.bs.collapse', () => {
      $('#more-delivery-dates-options').hide()
    })
  }
}

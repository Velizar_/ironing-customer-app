import {Controller} from "@hotwired/stimulus"
import {phoneIsValidUk, updateValidity} from "utils";

export default class extends Controller {
  onPhoneInput(_event) {
    const $phone = $('#phone')
    const mobilePhoneIsValid = phoneIsValidUk($phone.val(), false)
    updateValidity($phone, mobilePhoneIsValid === true)
    $('#invalid-phone').text(mobilePhoneIsValid)
    return mobilePhoneIsValid === true
  }

  connect() {
    const $phone = $('#phone')
    $('#link-form').submit(this.onPhoneInput)
    $phone.on('input', (_event) => {
      if ($phone.hasClass('is-invalid')) {
        this.onPhoneInput(_event)
      }
    })
  }
}

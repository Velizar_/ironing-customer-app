require 'net/https'

class BookingInfoFetcher
  def self.fetch(auth_token)
    uri = URI("#{ENV['MAIN_APP_URL']}/customer_app/booking_status.json")
    response = Net::HTTP.get_response(uri, 'Authorization' => auth_token)
    if !response.code.starts_with?('4') && !response.code.starts_with?('5') && response.body.present?
      JSON.parse response.body
    else
      Rails.logger.error("'Error in BookingInfoFetcher.fetch: with code #{response.code} - #{response.body}'")
      nil
    end
  end
end

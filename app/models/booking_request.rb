require 'net/https'

class BookingRequest
  def self.create(params, auth_token)
    uri = URI("#{ENV['MAIN_APP_URL']}/customer_app/request")
    response = Net::HTTP.post(uri, params.to_h.to_json, {
      'Content-Type' => 'application/json',
      'Authorization' => auth_token
    })
    if !response.code.starts_with?('4') && !response.code.starts_with?('5')
      response.body || 'Success'
    else
      Rails.logger.error("'Error in BookingRequest.create: with code #{response.code} - #{response.body}'")
      nil
    end
  end

  def self.get_booking_availability
    uri = URI("#{ENV['MAIN_APP_URL']}/customer_app/booking_availability.json")
    response = Net::HTTP.get_response(uri)
    if response.body.present?
      avl = JSON.parse response.body
      ['available', 'not_confirmed_as_available', 'unavailable', 'unconfirmed'].map do |key|
        avl[key].map { |d| d.to_date.strftime('%m/%d/%Y') }
      end
    else
      Rails.logger.error("'Error in BookingRequest.get_booking_availability: with code #{response.code} - #{response.body}'")
      'Failed to connect to server, please try again later'
    end
  end

  def self.delete(id, auth_token)
    uri = URI("#{ENV['MAIN_APP_URL']}/customer_app/request/#{id}")
    req = Net::HTTP.new(uri.host, uri.port)
    req.use_ssl = uri.scheme == 'https'
    response = req.delete(uri.path, 'Authorization' => auth_token)
    if !response.code.starts_with?('4') && !response.code.starts_with?('5')
      response.body || 'Success'
    else
      Rails.logger.error("'Error in BookingRequest.delete: with code #{response.code} - #{response.body}'")
      nil
    end
  end

  def self.delete_booking(id, auth_token)
    uri = URI("#{ENV['MAIN_APP_URL']}/customer_app/booking/#{id}")
    req = Net::HTTP.new(uri.host, uri.port)
    req.use_ssl = uri.scheme == 'https'
    response = req.delete(uri.path, 'Authorization' => auth_token)
    if !response.code.starts_with?('4') && !response.code.starts_with?('5')
      response.body || 'Success'
    else
      Rails.logger.error("'Error in BookingRequest.delete_booking: with code #{response.code} - #{response.body}'")
      nil
    end
  end
end

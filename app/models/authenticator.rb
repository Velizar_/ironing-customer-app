require 'net/https'

class Authenticator
  def self.login(phone, password)
    response = Net::HTTP.post(URI("#{ENV['MAIN_APP_URL']}/customers/sign_in.json"),
                              { customer: { phone: phone, password: password } }.to_json,
                              'Content-Type' => 'application/json')
    if response.code == '201'
      response['Authorization']
    else
      nil
    end
  end

  def self.logout(auth_token)
    uri = URI("#{ENV['MAIN_APP_URL']}/customers/sign_out.json")
    req = Net::HTTP.new(uri.host, uri.port)
    req.use_ssl = uri.scheme == 'https'
    req.delete(uri.path, 'Authorization' => auth_token)
  end
end

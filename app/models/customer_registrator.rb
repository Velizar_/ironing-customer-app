require 'net/https'
require 'bcrypt'

class CustomerRegistrator
  def self.create_code(phone)
    uri = URI("#{ENV['MAIN_APP_URL']}/customer_app/request_code")
    response = Net::HTTP.post_form(uri, phone: phone)
    if response.body.present? and response.code == '200'
      response.body
    elsif response.code == '429'
      ['Try again', response['Retry-After']]
    else
      Rails.logger.error("'Error in CustomerRegistrator.create_code: with code #{response.code} - #{response.body}'")
      'Failed to connect to server, please try again later'
    end
  end

  def self.check_code(code, phone)
    uri = URI("#{ENV['MAIN_APP_URL']}/customer_app/check_code")
    uri.query = URI.encode_www_form({ code: code, phone: phone })
    response = Net::HTTP.get_response(uri)
    if response.body.present? && !response.code.starts_with?('4') && !response.code.starts_with?('5')
      response.body
    else
      Rails.logger.error("'Error in CustomerRegistrator.check_code: with code #{response.code} - #{response.body}'")
      'Server error, please try again or contact us'
    end
  end

  def self.complete_account(code, phone, password)
    uri = URI("#{ENV['MAIN_APP_URL']}/customer_app/complete_account")
    response = Net::HTTP.post_form(uri, code: code, phone: phone, password: password)
    if response.body.present?
      response.body
    else
      Rails.logger.error("'Error in CustomerRegistrator.complete_account: with code #{response.code} - #{response.body}'")
      'Server error, please try again or contact us'
    end
  end

  def self.register_push_notification(parameters, auth_token)
    uri = URI("#{ENV['MAIN_APP_URL']}/customer_app/register_push_notification")
    response = Net::HTTP.post(uri, parameters.to_json, {
      'Content-Type' => 'application/json',
      'Authorization' => auth_token
    })
    if !response.code.starts_with?('4') && !response.code.starts_with?('5')
      response.body
    else
      Rails.logger.error("'Error in CustomerRegistrator.register_push_notification: with code #{response.code} - #{response.body}'")
      raise 'Error with registration, please try again later or contact us'
    end
  end

  def self.register(parameters)
    parameters['encrypted_password'] = BCrypt::Password.create(parameters['password'], cost: 10)
    parameters.delete('password')
    parameters['request_requirements'] = parameters['additional_requirements']
    parameters.delete('additional_requirements')
    uri = URI("#{ENV['MAIN_APP_URL']}/customer_app/register")
    response = Net::HTTP.post(uri, ({ customer_registration: parameters }).to_json, {
      'Content-Type' => 'application/json'
    })
    if response.body == 'Success'
      'Success'
    else
      Rails.logger.error("'Error in CustomerRegistrator.register: with code #{response.code} - #{response.body}'")
      nil
    end
  end
end
